# uauto-meta

This is a meta repository to note down the intentions and ideas behind the uauto project.

All written content is licensed under Creative Commons - Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) 

## Project

### Name

- uauto stands for 'union auto', 'ungal auto', 'unakaana auto'

### Inspiration

- Auto drivers are a group of individuals who cooperate within themselves for the collective benefit through the means of ride sharing, micro credit lending, group funds for medical / personal emergency or festival bonus. They also have the autonomy of a self employed job where they set their own timings, fees and clients.

- The way they exist, being attached to stands has created a situation where auto drivers willing to drive are separated from passengers willing to hire an auto. This vacuum / need has been solved by 'ride aggregation' services. Customers and drivers, both are happy in this scenario.

- Auto drivers don't have the skill or the time to acquire skill to develop their own ride aggregation technology. Therefore, they are reliant on service providers for their survival.

- The service provider sets their own terms and conditions for the drivers for when to drive, whom to drive for, what to charge and how much to drive. The service provider punishes the auto drivers for exercising their liberty. This situation is further worsened by the service provider inviting immigrants who do not have auto permits to drive for them, thereby creating competition for the auto drivers thus forcing them to accept any terms set by the service providers.

- Auto drivers are also forced to compete amongst themselves for rides thereby defeating their cooperation. This situation is existing in corporates and is the cause of economic slavery by corporates.

- We empathize and identify with their pain, therefore want to find a solution to it which is beneficial to the customers and the auto drivers.

### Objective

We aim -

- To empower auto drivers, the proactive unions supporting them to stand up and against corporates threatening their solidarity.

- To create a digital platform providing ride aggregation and allied location based services that help drivers and passengers connect.

- To provide the service at an affordable cost to the drivers, ensuring that they are able to work on their own terms.

- To empower the auto drivers union to manage themeselves better in a data driven manner.

- To respect and protect the privacy of the drivers, passengers, union and other stakeholders by design and implementation.

- To collect annonymized data in the domains of ride paths, points of entry, points of exit, time taken, recommended fare, condition of public infrastructure, real time incidents, GIS points etc.

- To use and the share the collected data with individual(s), group(s) of individuals and/or entities, who seek to use it for public welfare through the means of, but not limited to, research, activism, policy change, citizen science, social awareness, journalism, government programs. Commercial entities are required to seek permission and pay licensing fees to use this data.

### Arguments for uauto

#### For coop members

- Cooperative too, is a form of business. Every business succeeds only when it creates value for its clients. This value should be good enough that people are willing to pay for it. Value is created when people's needs are met.

- uauto is of dire need by the auto drivers. Ride aggregation in a fair, fast and secure manner is a requirement of the passengers. Labour unions require data to back their claims to the government, public and media. uauto is the perfect solution which could satisfy all of these needs in an optimum way, creating immense value to the stakeholders.

- We can use the collected data to expand our reach into location based services market and use it for social good.

- See financial feasibility

#### For drivers

- Ride aggregation services are here to staty.

- Subscription is way cheaper than percentage per ride. It doesn't feel like someone else is profitting from your hardwork.

- No daily targets. You are free to work as you please.

- We don't fix the fares. It is left to be between you and the passenger. We will however help the passenger make an informed decision.

- Free from driver harrassment. uauto doesn't exploit its drivers by charging exorbitant margins per ride and pressuring them to work for 16 hours a day. uauto cares about its drivers mental and financial well being. As drivers love working with uauto, they won't harass the passengers who are partners in their business.

#### For union

- Accurate and latest data on your members' financial health is required to address their needs and demands.

- uauto empowers auto drivers, their culture and the unions instead of creating competition and misery amongst them.

- Free from driver harrassment. uauto doesn't exploit its drivers by charging exorbitant margins per ride and pressuring them to work for 16 hours a day. uauto cares about its drivers mental and financial well being. As drivers love working with uauto, they won't harass the passengers who are partners in their business.

#### For passengers

- uauto uses latest improvements in Artificial Intelligence and Machine Learning to give realtime routing based on traffic, street smartness and time of the day for a fast and pleasant ride experience.

- Auto drivers have years of experience. They are registered with RTO, union and auto stand thus are reliable and accountable.

- Drivers are to be officially trained for Safety, Accident rescue & response.

- Free from driver harrassment. uauto doesn't exploit its drivers by charging exorbitant margins per ride and pressuring them to work for 16 hours a day. uauto cares about its drivers mental and financial well being. As drivers love working with uauto, they won't harass the passengers who are partners in their business.

- uauto doesn't share personal data with anyone. Internet services are known to share real time data on its users to ad services to earn profit. uauto collects anonymous data using state of the art security features to keep its users data private and secure.

### Financial Feasibility

#### Model 1

- Take lump sum from union for every month/year.

##### Advantages

- Fixed payment from union

##### Disadvantages

- Income is capped

#### Model 2

- Monthly subscription for every individual auto driver.

##### Advantages

- Income scales with number of drivers

##### Disadvantages

- Acquiring payment through cash is tedious

- Income can fluctuate

### Problems we are facing

- Money to finance / fund for a sustained runway

- Current effort from members not enough to achieve critical mass

- We so far have not devised a marketing strategy that has least expenditure and maximum effect.

### Points to focus on

- Market survey - tourists, residents, drivers

- Alternate revenue generation models - monthly subscription classifieds

- 1st phase of Coverage: Area of Interest now to Boulevard (needs to be discussed)

- 2nd phase of Coverage:

### Next steps

- Registration and Incorporation

- Devise a marketing strategy that which could not burn us (might start with pamphlet distribution - either direct or via newspapers ; social networking(fb, yt) ; website ; posters, dramas, banners, ad/catchy phrases behind auto's about pride of auto's itslef, local television, FM, Newspaper Journlism, Digital Journalism)

## Features

### Safety Features

#### Accident Rescue, Report

- SOS, Reporting to relevant authorities

- Collection of first hand data

- Routing away of incoming uauto traffic

### Map features

#### Extra information

- Classifieds / Ads

- Tourist guides

- Events, push notification to drivers

#### Preset

- Split maps into multiple presets - Tourist, commercial, resident

- Allow configuration of various PoI types for customization

#### Styling

- Allow multiple map styles

#### Voluntary data contribution

- Quality of roads, public infrastructure

- OSM Contribution through StreetComplete like interface ?

### Service features

- Realtime routing

### UI Features

- Cross language chat wheel - online and offline

## Toolchain

- E2E toolchain for ethical ride aggregator service

- Setting up should be similar to mailinabox

- Acquire data from OSM

- Package / render data for android and web render

## Web Application

### Ride aggregation

### Avail ride

### Statistics

### Public Data

## Android

### Avail Ride

### Drive

### SOS

### Cross-language chat wheel

## v1.0 Roadmap

### Phase 1

- Liveline - May 1

- Web & Android app with ride aggregation

### Phase 1.5

- Map data render & deploy toolchain

### Phase 2

- Map layer features

## Extra Features

- Safety training for auto drivers.

- Translation of queries.

- Connect tourist guides

## Inforgraphics

-
