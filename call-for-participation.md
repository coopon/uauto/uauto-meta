### Dear Free Software Hackers,

I am writing this letter as a call for **action**. As free software hacktivists, in the beginning we spent all our time and energy advocating the idea of Free(dom) Software, helpled people migrate from proprietary operating systems and tools to use FLOSS systems and tools. Soon the idea of FLOSS was picked up by hardware enthusiasts as well and we saw the birth of Open Hardware Movement which advocated sharing of hardware designs with others and also provide them with the same rights to modify and reuse those designs.

While we were on these fronts, new challenges came up. The challenges were about defending the Neutrality of the Internet, fighting against the draconian acts in the legal framework, commodification of our social relationships, defending our fundamental right to privacy, etc., In short, our operating scope became wider.

In the past and also in the present, we seek, build and advocate for alternatives. Alternatives to unethical and exploitative models of digital systems and tools. It is the free software hackers around the world brought GNU/Linux, Mediawiki (Wikipedia), F-Droid, Arduino, Mastodon, Peertube, VLC, Nextcloud, Riot and a lot of other FLOSS tools and hardware designs.

In the realm of knowledge freedom, we cannot forget the struggle of many hacktivists to liberate the knowledge from paywalls & proprietary lockdown including late Aaron Swartz, whom we have lost in the fight. Today we have OpenAccess, Creative Commons, OpenData, Sci-hub, libgen, etc.,

The dynamic exhibition of what is possible when people come together collectively to democratize production has spread across other domains as well. Soon there were many initiatives across the globe, which includes "Free Libre and Open Knowledge" based soceities called "FLOK Society" in short at Eucador, a new kind of experimental production based on peer to peer systems called Peer production, platform cooperativism, etc., We should understand that these are all on-going experiments and whether they will succeed or fail, we cannot speculate now.

With this context, today, in India as well as in other countries across the world, we are seeing continuous protests and struggle of cab and auto-rickshaw drivers against the exploitative work hours and drivers slowly losing control over their working options. Protest after protest they were not able to win back any of their demands (or) strengthen their fight because they are trapped between Ola/Uber when it comes to these ride aggregation platforms.

I personally came to know about some members of taxi driver unions were wasting lakhs of money asking some private small scale software vendors to develop a ride aggreagtion platform for them so that they can they can opt out of Ola/Uber. But what they don't understand is that yet another closed source and walled garden platform cannot become an alternative to Ola/Uber.

The general secretary of FSMI, Mr. Kiran Chandra addressed during FSMI's national hacktivist camp 2018, building such an alternative platforms requires building a stronger narrative among the general public and among the drivers as well, because it is these two entities who come together to use this service. Without forging such an alliance between the people and the drivers, and without having a FLOSS / commons based and federated service (like PeerTube, read about their strategy on how to take at a centralized walled garden service like Youtube and also about ActivityPub protocol) for ride aggregation, an alternative cannot be envisioned.

What if, there exist a Free Software platform with server side licensed under AGPL and the client side (web and mobile) licensed under GPL that integrated OpenStreetMap for geographical data and VTM (Mapsforge) as offline map rendering and Graphhopper as offline routing engine therefore minimizng the costs of operating a server and also the servers themselves federated among each other? There were would be 

Existence of such a platform could then be used by the collective of drivers themselves as unions and confederation of unions to deploy across the nation and operate. If we subscribe to the ideals of Free Software and if we understand that free software is a kind of software that is built by the people, of the people and for the people, then we should not hesitate to come forward to build such alternative platforms when a section of people are in such a need.

This can also become an opportunity for some of us in the free software movement to self-employ ourselves along with unions to operate this platform as a service in a cooperative model. There could also be other modes of operation as well, but that is for later discussion, first we need to overcome the challenge of not having such a platform ready, only then we shall all think about operating it.

Therefore, I am calling the free software hacktivists, developers, etc., to come together to build such a platform.

### Who are required?

 - UI/UX Designers.
 - GIS (Geographical Information System) developers.
 - Software and System Architects.
 - Backend (Server-side) and Frontend developers.
 - Android App developers.
 - Localization and Translators.
 - OpenStreetMap contributors.


### What has been attempted?

#### Android

A few of us from free software movement at Pondicherry  have experimented building an android app that can operate offline for rendering Vector Map offline with OpenStreetMap data as base, that can also perfom offline routing with Graphhopper routing engine in Android.

The source for the same can be found here 

#### Backend

Not much has been done on the backend, due to the lack of clarity in the backend architecture. But a minimal RESTful API has been attempted in multiple languages (Go, Rust and Python).

Link to those repositories can be found below,

  - https://gitlab.com/coopon/uauto
  
For further reading on the issues of drivers and their struggles,

The website https://tnlabour.in has documented the struggles of drivers in TamilNadu to a very good extent. Therefore you may find most of the links comes from their site.

 - Feb 2019, TN, https://tnlabour.in/news/8087
 - October 2018, TN, https://tnlabour.in/news/7501
 - October 2018, Maharashtra, https://scroll.in/latest/899293/ola-uber-taxi-drivers-continue-their-strike-in-mumbai-to-demand-better-fares-incentives
 - June 2018, TN, https://tnlabour.in/news/6926
 - March 2018, AP/Telengana, http://www.newindianexpress.com/cities/hyderabad/2018/mar/18/uber-ola-in-hyderabad-to-join-nationwide-indefinite-strike-from-today-1788875.html
 - Jan 2018, TN, https://tnlabour.in/news/6262
 - March 2017, TN, https://tnlabour.in/news/4912
 - Feb 2017, Karnataka, https://www.vccircle.com/ola-uber-services-affected-drivers-go-strike-bangalore/
 
You can also search and read on what's happening at International level as well. Similar strikes and protests and following which an alternative cooperative model of operation is also attempted.
 
  
For further reading on building alternatives as cooperatives,

 - http://www.rosalux-nyc.org/platform-cooperativism-2/
 - http://sewa.org/
 - https://gitlab.com/fshm/awesome-coop


### I am Interested, How and Where to Join?

I guess, we can have a separate mailing list, a repository on gitlab (or) any other FLOSS hosting service, a riot/matrix group for IMs, etc. but before proceeding there, please fill out this form here and we shall proceed from there.

Criticisms, feedbacks and suggestions on this proposal are welcome.
